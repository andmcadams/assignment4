name: Andrew McAdams
eid: am73849
bitbucketid: andmcadams
Jenkins URL: http://ec2-54-197-66-250.compute-1.amazonaws.com:8080
Application URL: http://35.227.192.139
comments: 
Year is restricted to positive integers.
Rating is restricted to numbers between 0.0 and 5.0 (inc). If a rating is more than two decimal places, it will be rounded to two places.
If there are multiple movies with the same title, they will ALL be updated or ALL be deleted depending on the action.
If there are multiple errors in the same request, they will all be displayed, each on its own line.