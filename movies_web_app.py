import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application

#Test comment
def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies (id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title TEXT, director TEXT, actor TEXT, release_date DATE, rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)

# Extended to allow all searches required for this application.
def query_data(search='', condition='TRUE', extra_args=''):

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT {select} FROM movies WHERE {condition} {extra_args}".format(select=search, condition=condition, extra_args=extra_args))
    entries = cur.fetchall()
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

# Runs an arbitrary SQL command.
def run_mysql_com(command=''):
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute(command)
    cnx.commit()


"""
You can assume that all the inputs will always be provided, there will be no empty input.

(year, title, director, actor, release_date, rating )
FAILURE CASES:
Year is malformed.
Release_Date is malformed.
Rating is malformed.

NOTES:
Title, director, and actor are considered valid as long as they are not blank (which we are assuming they aren't).
Although it's unlikely --3h2eqfS><@\x80\x10 is an actor, I'm just going to let it slide.
"""

# On return, success is false if at least one piece of data failed validation.
# When success is false, message is set to a failure message. The other values in the tuple are unspecified.
# On return, success is true iff all data is valid.
# When success is true, message is blank, t is the release date formatted as a string for SQL input,
# y is a valid year (four digits), and rating is the given rating rounded to two decimals. 
add_fail = "Movie '{title}' could not be {action} - Illegal value for {field}: {value}"
add_success = "Movie '{title}' successfully {action}"
def validate_movie_data(r, action):
    success = False
    messages = []
    r = request.form
    t=''
    y='' 
    rating=''
    try:
        t = time.strptime(r['release_date'], "%d %b %Y")
        t = "{}-{}-{}".format(t.tm_year, t.tm_mon, t.tm_mday)
    except ValueError as v:
        messages.append(add_fail.format(title=r['title'], action=action, field="release date", value=r['release_date']))
        t = ''
        print v

    # This will fail if the year is not a four digit number. Since the first motion picture was made in 1888
    # and it seems unlikely that my assignment will be used almost 8000 years from now, I'm sticking with this.
    # Alternatively, I could just make sure it is an integer (preferably positive).
    try:
        y = int(r['year'])

        # This is a coarse check. Obviously this is not very useful if someone is entering bad years.
        if y < 0:
            raise ValueError
    except ValueError as v:
        messages.append(add_fail.format(title=r['title'], action=action, field="year", value=r['year']))
        y = ''
        print v

    try:
        rating = float(r['rating'])

        # Mentioned on Piazza to restrict ratings to 2 decimal places. Instead of returning an error if
        # the rating has more than that, I'm just rounding it. Seems silly to return an error if inputting 5.000
        rating = round(rating, 2)

        # No range for the values of rating were given, so 0-5 was arbitrarily selected.
        if rating < 0 or rating > 5:
            raise ValueError

    except ValueError as v:
        messages.append(add_fail.format(title=r['title'], action=action, field="rating", value=r['rating']))
        rating = ''
        print v
    if t and y and rating:
        success = True

    message = '<br>'.join(messages)

    return (success, message, t, y, rating)


"""
What I am really doing here is 
1) validating the given data
2) if it is valid, insert the movie
"""
@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    r = request.form
    (success, message, t, y, rating) = validate_movie_data(request.form, "inserted")
    if success:  
        run_mysql_com("INSERT INTO movies (year, title, director, actor, release_date, rating ) values ('{}', '{}', '{}', '{}', '{}', '{}')".format(y, r['title'], r['director'], r['actor'], t, rating))
        message = add_success.format(title=r['title'], action="inserted")
    return hello(message)

"""
What I am really doing here is 
1) checking to make sure the movie exists via selecting for its title
2) validating the new data
3) if it is valid, update the movie (or all entries with that title)
"""
update_fail = "Movie '{title}' could not be {action} - No movie found with title {title}"
@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    r = request.form
    movies = query_data(search='title', condition="title = '{}'".format(request.form['title']))
    if movies:
        (success, message, t, y, rating) = validate_movie_data(request.form, "updated")
        if success:
            run_mysql_com("UPDATE movies SET year = '{}', director = '{}', actor = '{}', release_date = '{}', rating = '{}' WHERE title = '{}'".format(y, r['director'], r['actor'], t, rating, r['title']))
            message = add_success.format(title=movies[0][0], action="updated")
    else:
        message = update_fail.format(title=request.form['title'], action="updated")
    return hello(message)

"""
What I am really doing here is 
1) checking to make sure the movie exists via selecting for its title
2) if it does, delete all movies with that title
"""
delete_success = "Movie '{title}' successfully deleted"
delete_fail = "Movie with title '{title}' does not exist"
@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    r = request.form
    movies = query_data(search='title', condition="title = '{}'".format(r['delete_title']))
    if movies:
        run_mysql_com("DELETE FROM movies WHERE title = '{}'".format(r['delete_title']))
        message = delete_success.format(title=movies[0][0])
    else:
        message = delete_fail.format(title=r['delete_title'])

    return hello(message)

"""
What I am really doing here is 
1) checking to make sure a movie exists with the given actor
2) if it does, display all movies with that actor
"""
@app.route('/search_movie', methods=['GET'])
def search_movie():
    actor = request.args.get('search_actor')
    movies = query_data(search='title, year, actor', condition="actor = '{}'".format(actor))
    if movies:
        message = ''
        for movie in movies:
            message += "{}, {}, {}<br>".format(movie[0], movie[1], movie[2])
    else:
        message = "No movies found for actor '{}'".format(actor)
    return hello(message)

"""
Note that the code in either case is identical with the exception of ASC or DESC ordering of the table by rating.
Thus the two buttons route to the same function, with different endpoints.
What I am really doing here is 
1) figuring out which endpoint called this function
2) finding the high/lowest ranked movie
3) if a movie is found, return all movies with the same rating
4) otherwise, there are no movies in the database
"""
rating_fail = "There are no movies in the database"
@app.route('/highest_rating', endpoint='highest_rating', methods=['GET'])
@app.route('/lowest_rating', endpoint='lowest_rating', methods=['GET'])
def highest_rating():
    print request.endpoint
    if request.endpoint == 'highest_rating':
        order = "DESC"
    else:
        order = "ASC"
    ranked_movie = query_data(search='title, year, actor, director, rating', extra_args="ORDER BY rating {order} LIMIT 1".format(order=order))
    if ranked_movie:
        movies = query_data(search='title, year, actor, director, rating', condition="rating = '{rating}'".format(rating=ranked_movie[0][4]))
        message = ''
        if movies:
            for movie in movies:
                message += "{}, {}, {}, {}, {}<br>".format(movie[0], movie[1], movie[2], movie[3], movie[4])
        else:
            message = rating_fail
    else:
        message = rating_fail

    return hello(message)

@app.route("/")
def hello(message=''):
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    return render_template('index.html', message=message)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
